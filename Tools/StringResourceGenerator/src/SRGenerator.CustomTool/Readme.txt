Registration:
c:/> regasm /codebase SRCodeGenerator.CustomTool.dll

Unregistration:
c:/> regasm /unregister SRCodeGenerator.CustomTool


Or use the installer.

Usage:
Add an .resx file to the project and set:
 Custom Tool: SRCodeGen

Or Add a SR.strings file to the project and set:
 Custom Tool: StringResourceTool

See:
	http://wah.onterra.net/blog/archive/2004/12/09/190.aspx

for more details.



