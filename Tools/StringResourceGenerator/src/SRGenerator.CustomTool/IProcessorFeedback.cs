using System;

namespace SRGenerator.CustomTool
{
	public interface IProcessorFeedback
	{
        void LineError(string message, string line,int lineNo);
        void Information(string message);
        void AddResourceFile(string filename);
	}
}
