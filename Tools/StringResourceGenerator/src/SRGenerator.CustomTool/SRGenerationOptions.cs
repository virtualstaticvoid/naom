namespace SRGenerator.CustomTool
{
  public class SRGenerationOptions
  {
    public bool PublicSRClass;
    public string CultureInfoFragment;
    public string ResourceFileNamespace;
    public string[] AdditionalUsing = new string[] {};
  }
}
