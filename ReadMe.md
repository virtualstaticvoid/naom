## Welcome to NAom!

NAom is an implementation of the Adaptive Object Model (AOM) architecture for dynamic applications.

AOM allows for the dynamic creation of entities, their properties and functionality, without any changes
to the code and/or storage schema, at runtime.
This is well suited to applications where end-users define the entities, their properties and operations
within the application, instead of a programmer.

Some example application domains that can benefit from this approach include:

* Financial Analytics
* Medical Systems
* User-defined Product Systems
* Customer Relationship Management
* Data Portals

The NAom library is still a work in progress, so it doesn't fully implement the AOM specification yet.

The following C# code snippets illustrate the usage.

```csharp
PropertyTypeInt32 propertyTypeId;
PropertyTypeString propertyTypeCode;
PropertyTypeString propertyTypeDescription;
PropertyTypeBool propertyTypeActive;

// define the entity type and it's properties
DynamicType<object> dynamicType = new DynamicType<object>
  (
    "Sample1",
    propertyTypeId = new PropertyTypeInt32("Id"),
    propertyTypeCode = new PropertyTypeString("Code"),
    propertyTypeDescription = new PropertyTypeString("Description"),
    propertyTypeActive = new PropertyTypeBool("Active", true)
  );

// create the instance factory
IDynamicTypeFactory<object> factory = dynamicType.CreateFactory();

// create the instance factory
object instance = factory.CreateInstance();

// assign a property
instance.SetProperty(propertyTypeId, 2);

// retrieve property values
int id = instance.GetProperty(propertyTypeId);
string code = instance.GetProperty(propertyTypeCode);
string description = instance.GetProperty(propertyTypeDescription);
bool active = instance.GetProperty(propertyTypeActive);
```

Also, interfaces can be used to allow for scenarios where some of the property types are always required,
or are part of the domain.
The requirement is that the interface only has properties and their names and data types correspond to some
(or all) of the property types supplied at runtime.

Given the following interface:

```csharp
public interface INamedItem
{
  string Name { get; set; }
}
```

The following code snippet illustrates how it ties in:

```csharp
PropertyTypeString propertyTypeDescription;

// define the entity type and it's properties
DynamicType<INamedItem> dynamicType = new DynamicType<INamedItem>
  (
    "Sample2",
    new PropertyTypeString("Name"),
    propertyTypeDescription = new PropertyTypeString("Description")
  );

// create the instance factory
IDynamicTypeFactory<INamedItem> factory = dynamicType.CreateFactory();

// create a new instance
INamedItem instance = factory.CreateInstance();

// now the "dynamic" property is directly accessible
instance.Name = "ABC";

// "unknown" properties WRT the interface are still accessible!
instance.SetProperty(propertyTypeDescription, "You there?");
string description = instance.GetProperty(propertyTypeDescription);
```

Also, take a look at the prototype projects in the source code, which explore various techniques of AOM,
including using Linq to SQL.

[Christopher G Lasater](http://www.codeproject.com/script/Membership/Profiles.aspx?mid%3D314543) has written
a series of [articles](http://www.codeproject.com/KB/architecture/AOM_Property.aspx) on the CodeProject
website on the subject.

Also, a detailed [document](http://www.adaptiveobjectmodel.com/OOPSLA2001/AOMIntriguingTechPaper.doc),
by Joseph Yoder, Ralph E. Johnson et al, discusses the AOM approach, and makes for interesting reading.

More information on the subject can be found in the [article](http://www.codeproject.com/KB/architecture/AOM_Property.aspx)
by [Christopher G Lasater](http://www.codeproject.com/script/Membership/Profiles.aspx?mid%3D314543) on
the [CodeProject](http://www.codeproject.com) website.

The [Joe's MetaData and Adaptive Object-Model](http://adaptiveobjectmodel.com) website is entirely devoted to
the subject.

Also, there is a great blog post entitled [The Universal Design Pattern](http://steve-yegge.blogspot.com/2008/10/universal-design-pattern.html)
by [Steve Yegge](http://steve-yegge.blogspot.com) which makes mention of this approach.

Further reading includes the following books/articles:

* James Noble: The Object System Pattern, submitted to EuroPLoP 98, 1998
* Marcel Rüedi, Peter Sommerlad: Named Object, Proto-Pattern, submitted to Writing Workshop at EuroPLoP 98, IFA Informatik 1998
* Pattern Languages of Program Design 3, Addison-Wesley, 1997
* Dirk Riehle: A Role-Based Design Pattern Catalog of Atomic and Composite Patterns Structured by Pattern Purpose, UbiLab Tecnical Report 97.1.1, 1996
* F. Buschmann, R. Meunier, H. Rohnert, P. Sommerlad, M. Stal: Pattern Oriented Software Architecture - A System of Patterns, J. Wiley & Sons, 1996
* Kai-Uwe Mätzel, Walter Bischofberger: The Any Framework A Pragmatic Approach to Fleibility, USENIX COOTS Toronto, 1996
* E. Gamma, R. Helm, R. Johnson, J. Vlissides: Design Patterns &amp; Elements of Reusable Object-Oriented Software, Addison-Wesley, 1995
* Kent Beck: Smalltalk Best Practice Patterns, Addison-Wesley, 1995
