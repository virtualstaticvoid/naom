﻿#region Copyright
// 
// Copyright (C) 2008 VirtualStaticVoid <virtualstaticvoid@gmail.com>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#endregion

using System;
using NUnit.Framework;

namespace NAom.Core.Tests
{

  [TestFixture]
  public class RatingTests
  {

    [Test]
    public void TestRatings()
    {

      DynamicType<IInstrument> instrumentDynamicType = new DynamicType<IInstrument>("Instrument");
      IDynamicTypeFactory<IInstrument> instrumentFactory = instrumentDynamicType.CreateFactory();
      IInstrument instrument = instrumentFactory.CreateInstance();
      
      DynamicType<IIssuer> issuerDynamicType = new DynamicType<IIssuer>("Issuer");
      IDynamicTypeFactory<IIssuer> issuerFactory = issuerDynamicType.CreateFactory();
      IIssuer issuer = issuerFactory.CreateInstance();

      DynamicType<IRatingAgency> ratingAgencyDynamicType = new DynamicType<IRatingAgency>
        (
          "RatingAgency",
          new PropertyType<string>("Code")
        );

      IDynamicTypeFactory<IRatingAgency> ratingAgencyFactory = ratingAgencyDynamicType.CreateFactory();
      IRatingAgency ratingAgency = ratingAgencyFactory.CreateInstance();


      DynamicType<IRatingAgencyRating> ratingAgencyRatingDynamicType = new DynamicType<IRatingAgencyRating>
        (
          "RatingAgencyRating",
          new PropertyType<IRatingAgency>("RatingAgency"),
          new PropertyType<RatingTerm>("RatingTerm"),
          new PropertyType<RatingType>("RatingType"),
          new PropertyType<string>("Rating"),
          new PropertyType<int>("Rank")
        );

      IDynamicTypeFactory<IRatingAgencyRating> ratingAgencyRatingFactory = ratingAgencyRatingDynamicType.CreateFactory();

      IRatingAgencyRating ratingAgencyRating = ratingAgencyRatingFactory.CreateInstance();
      ratingAgencyRating.RatingAgency = ratingAgency;
      ratingAgencyRating.RatingTerm = RatingTerm.ShortTerm;
      ratingAgencyRating.RatingType = RatingType.National;
      ratingAgencyRating.Rating = "F1+";
      ratingAgencyRating.Rank = 999;


      DynamicType<IIssuerRating> issuerRatingDynamicType = new DynamicType<IIssuerRating>
        (
          "IssuerRating",
          new PropertyType<IRatingAgencyRating>("RatingAgencyRating"),
          new PropertyType<IIssuer>("Entity")
        );

      IDynamicTypeFactory<IIssuerRating> issuerRatingFactory = issuerRatingDynamicType.CreateFactory();
      IIssuerRating issuerRating = issuerRatingFactory.CreateInstance();
      issuerRating.RatingAgencyRating = ratingAgencyRating;
      issuerRating.Entity = issuer;

      DynamicType<IInstrumentRating> instrumentRatingDynamicType = new DynamicType<IInstrumentRating>
        (
          "InstrumentRating",
          new PropertyType<IRatingAgencyRating>("RatingAgencyRating"),
          new PropertyType<IInstrument>("Entity")
        );

      IDynamicTypeFactory<IInstrumentRating> instrumentRatingFactory = instrumentRatingDynamicType.CreateFactory();
      IInstrumentRating instrumentRating = instrumentRatingFactory.CreateInstance();
      instrumentRating.RatingAgencyRating = ratingAgencyRating;
      instrumentRating.Entity = instrument;


      DynamicType<IRatingScale> ratingScaleDynamicType = new DynamicType<IRatingScale>
        (
          "RatingScale",
          new PropertyType<string>("Code"),
          new PropertyType<string>("Description"),
          new PropertyType<int>("DefaultBand")
        );

      IDynamicTypeFactory<IRatingScale> ratingScaleFactory = ratingScaleDynamicType.CreateFactory();
      IRatingScale ratingScale = ratingScaleFactory.CreateInstance();

      DynamicType<IRatingScaleBand> ratingScaleBandDynamicType = new DynamicType<IRatingScaleBand>
        (
          "RatingScaleBand",
          new PropertyType<IRatingScale>("RatingScale"),
          new PropertyType<IRatingAgencyRating>("RatingAgencyRating"),
          new PropertyType<int>("Band")
        );

      IDynamicTypeFactory<IRatingScaleBand> ratingScaleBandFactory = ratingScaleBandDynamicType.CreateFactory();
      IRatingScaleBand ratingScaleBand = ratingScaleBandFactory.CreateInstance();
      ratingScaleBand.RatingScale = ratingScale;
      ratingScaleBand.RatingAgencyRating = ratingAgencyRating;
      ratingScaleBand.Band = 1;

    }

    public interface IIssuer
    {
    }

    public interface IInstrument
    {
    }

    public enum RatingTerm
    {
      ShortTerm,
      LongTerm,
    }

    public enum RatingType
    {
      National,
      International,
    }

    public interface IRatingAgency
    {
      string Code { get; }
    }

    public interface IRatingAgencyRating
    {
      IRatingAgency RatingAgency { get; set; }
      RatingTerm RatingTerm { get; set; }
      RatingType RatingType { get; set; }
      string Rating { get; set; }
      int Rank { get; set; }
    }

    public interface IRatedEntity<TEntity>
    {
      IRatingAgencyRating RatingAgencyRating { get; set; }
      TEntity Entity { get; set; }
    }

    public interface IIssuerRating : IRatedEntity<IIssuer>
    {
    }

    public interface IInstrumentRating : IRatedEntity<IInstrument>
    {
    }

    public interface IRatingScale
    {
      string Code { get; set; }
      string Description { get; set; }
      int DefaultBand { get; set; }
    }

    public interface IRatingScaleBand
    {
      IRatingScale RatingScale { get; set; }
      IRatingAgencyRating RatingAgencyRating { get; set; }
      int Band { get; set; }
    }

  }

}
