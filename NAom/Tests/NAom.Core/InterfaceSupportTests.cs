﻿#region Copyright
// 
// Copyright (C) 2008 VirtualStaticVoid <virtualstaticvoid@gmail.com>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#endregion

using NUnit.Framework;

namespace NAom.Core.Tests
{

  [TestFixture]
  public class InterfaceSupportTests
  {

    [SetUp]
    public void Setup()
    {
    }

    [TearDown]
    public void Teardown()
    {
    }

    [Test]
    public void DerivationOfPropertyTypeFromInterface()
    {
      IPropertyType[] propertyTypes =
        InterfaceSupport.DerivePropertyTypes(typeof(IInterfaceWithProperties));
      Assert.AreEqual(1, propertyTypes.Length);
      Assert.AreEqual("SomeProp", propertyTypes[0].Name);
      Assert.AreEqual(typeof(string), propertyTypes[0].DataType);
    }

    [Test]
    public void DerivationOfPropertyTypeFromInterfaceBasedOnGenericInterface()
    {
      IPropertyType[] propertyTypes =
        InterfaceSupport.DerivePropertyTypes(typeof(IInterfaceBasedOnGenericInterface));
      Assert.AreEqual(1, propertyTypes.Length);
      Assert.AreEqual("SomeProp", propertyTypes[0].Name);
      Assert.AreEqual(typeof(string), propertyTypes[0].DataType);
    }

  }
}
