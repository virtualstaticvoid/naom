﻿#region Copyright
// 
// Copyright (C) 2008 VirtualStaticVoid <virtualstaticvoid@gmail.com>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#endregion

using System;
using NUnit.Framework;

namespace NAom.Core.Tests
{

  [TestFixture]
  public class WikiSamples
  {

    [Test]
    public void TestSamples()
    {
      Sample1();
      Sample2();
    }

    public static void Sample1()
    {

      PropertyType<int> propertyTypeId;
      PropertyType<string> propertyTypeCode;
      PropertyType<string> propertyTypeDescription;
      PropertyType<bool> propertyTypeActive;

      // define the entity type and it's properties
      DynamicType<object> dynamicType = new DynamicType<object>
        (
          "Sample1",
          propertyTypeId = new PropertyType<int>("Id"),
          propertyTypeCode = new PropertyType<string>("Code"),
          propertyTypeDescription = new PropertyType<string>("Description"),
          propertyTypeActive = new PropertyType<bool>("Active", true)
        );

      // create the instance factory
      IDynamicTypeFactory<object> factory = dynamicType.CreateFactory();

      // create the instance factory
      object instance = factory.CreateInstance();

      // assign a property
      instance.SetProperty(propertyTypeId, 2);

      // retrieve property values
      int id = instance.GetProperty(propertyTypeId);
      string code = instance.GetProperty(propertyTypeCode);
      string description = instance.GetProperty(propertyTypeDescription);
      bool active = instance.GetProperty(propertyTypeActive);
			
	  Console.WriteLine(id);
	  Console.WriteLine(code);
	  Console.WriteLine(description);
	  Console.WriteLine(active);
    }

    public interface INamedItem
    {
      string Name { get; set; }
    }

    public static void Sample2()
    {

      PropertyType<string> propertyTypeDescription;

      // define the entity type and it's properties
      DynamicType<INamedItem> dynamicType = new DynamicType<INamedItem>
        (
          "Sample2",
          new PropertyType<string>("Name"),
          propertyTypeDescription = new PropertyType<string>("Description")
        );

      // create the instance factory
      IDynamicTypeFactory<INamedItem> factory = dynamicType.CreateFactory();

      // create a new instance
      INamedItem instance = factory.CreateInstance();

      // now the "dynamic" property is directly accessible
      instance.Name = "ABC";

      // "unknown" properties WRT the interface are still accessible!
      instance.SetProperty(propertyTypeDescription, "You there?");
      string description = instance.GetProperty(propertyTypeDescription);
			
	  Console.WriteLine(description);
      
    }

  }
}
