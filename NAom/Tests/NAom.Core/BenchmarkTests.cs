﻿#region Copyright
// 
// Copyright (C) 2008 VirtualStaticVoid <virtualstaticvoid@gmail.com>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#endregion

using NUnit.Framework;

namespace NAom.Core.Tests
{

  [TestFixture]
  public class BenchmarkTests
  {

    [Test]
    public void TestBenchmarks()
    {

      DynamicType<IBenchmark> benchmarkDynamicType = Support.MakeDynamicType<IBenchmark>();
      DynamicType<IInstrumentBenchmarkConstituent> instrumentBenchmarkConstituentDynamicType = Support.MakeDynamicType<IInstrumentBenchmarkConstituent>();

      IDynamicTypeFactory<IBenchmark> benchmarkFactory = benchmarkDynamicType.CreateFactory();
      IDynamicTypeFactory<IInstrumentBenchmarkConstituent> instrumentBenchmarkConstituentFactory = instrumentBenchmarkConstituentDynamicType.CreateFactory();

      IBenchmark benchmark = benchmarkFactory.CreateInstance();
      IInstrumentBenchmarkConstituent instrumentBenchmarkConstituent = instrumentBenchmarkConstituentFactory.CreateInstance();

      instrumentBenchmarkConstituent.Benchmark = benchmark;
      instrumentBenchmarkConstituent.Weighting = 100;

    }

    public interface IInstrument
    {
    }

    public interface IIssuer
    {
    }

    public interface IBenchmark
    {
      string Code { get; set; }
      string Description { get; set; }
    }

    public interface IBenchmarkConstituent<TEntity>
    {
      IBenchmark Benchmark { get; set; }
      TEntity Entity { get; set; }
      double Weighting { get; set; }
    }

    public interface IInstrumentBenchmarkConstituent : IBenchmarkConstituent<IInstrument>
    {
    }

    public interface IIssuerBenchmarkConstituent : IBenchmarkConstituent<IIssuer>
    {
    }

  }

}
