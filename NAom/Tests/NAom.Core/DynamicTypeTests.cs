﻿#region Copyright
// 
// Copyright (C) 2008 VirtualStaticVoid <virtualstaticvoid@gmail.com>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#endregion

using System;
using System.Reflection;
using NUnit.Framework;

namespace NAom.Core.Tests
{

  [TestFixture]
  public class DynamicTypeTests
  {

		[SetUp]
		public void Setup()
		{
		}

		[TearDown]
		public void Teardown()
		{
		}

    [Test]
    [ExpectedException(typeof(NAomException))]
    public void PrivateTypesNotSupported()
    {
      new DynamicType<PrivateClass>();
    }

    [Test]
    public void DynamicTypeBasedOnInterface()
    {
      DynamicType<IInterface> dynamicType = new DynamicType<IInterface>();
      Assert.AreEqual(dynamicType.BaseType, typeof (IInterface));
      
      var factory = dynamicType.CreateFactory();
      Assert.IsNotNull(factory);
      
      var instance = factory.CreateInstance();
      Assert.IsNotNull(instance);
    }

    [Test]
    public void DynamicTypeBasedOnAbstractClass()
    {
      DynamicType<AbstractClass> dynamicType = new DynamicType<AbstractClass>();
      Assert.AreEqual(dynamicType.BaseType, typeof (AbstractClass));
      
      var factory = dynamicType.CreateFactory();
      Assert.IsNotNull(factory);
      
      var instance = factory.CreateInstance();
      Assert.IsNotNull(instance);
    }

    [Test]
    [ExpectedException(typeof(TypeLoadException))]
    public void DynamicTypeBasedOnAbstractClassWithAbstractMethod()
    {

      // can't support abstract types with abstract members
      //  unless those members are properties which correspond with the property types given

      DynamicType<AbstractClassWithAbstractMethod> dynamicType = new DynamicType<AbstractClassWithAbstractMethod>();
      
      var factory = dynamicType.CreateFactory();
      Assert.IsNotNull(factory);

      var instance = factory.CreateInstance();
      Assert.IsNotNull(instance);

    }

    [Test]
    public void DynamicTypeBasedOnAbstractClassWithAbstractProperty()
    {
      DynamicType<AbstractClassWithAbstractProperty> dynamicType = new DynamicType<AbstractClassWithAbstractProperty>();
      dynamicType.Properties.Add(new PropertyType<string>("SomeProp"));
      
      var factory = dynamicType.CreateFactory();
      Assert.IsNotNull(factory);

      var instance = factory.CreateInstance();
      Assert.IsNotNull(instance);
    }

    [Test]
    public void DynamicTypeBasedOnConcreteClass()
    {
      DynamicType<ConcreteClass> dynamicType = new DynamicType<ConcreteClass>();
      Assert.AreEqual(dynamicType.BaseType, typeof (ConcreteClass));
      
      var factory = dynamicType.CreateFactory();
      Assert.IsNotNull(factory);

      var instance = factory.CreateInstance();
      Assert.IsNotNull(instance);
    }

    [Test]
    public void DynamicTypeBasedOnInterfaceWithProperties()
    {
      DynamicType<IInterfaceWithProperties> dynamicType = new DynamicType<IInterfaceWithProperties>();
      Assert.AreEqual(dynamicType.BaseType, typeof(IInterfaceWithProperties));
      dynamicType.Properties.Add(new PropertyType<string>("SomeProp"));
      
      var factory = dynamicType.CreateFactory();
      Assert.IsNotNull(factory);

      var instance = factory.CreateInstance();
      Assert.IsNotNull(instance);
    }

    [Test]
    public void DynamicTypeBasedWithDifferentTargetAndBaseType()
    {
      DynamicType<IInterfaceWithProperties> dynamicType = new DynamicType<IInterfaceWithProperties>("Test", typeof(AbstractClass));

      Assert.AreEqual(dynamicType.BaseType, typeof(AbstractClass));
      Assert.AreNotEqual(dynamicType.BaseType, typeof(IInterfaceWithProperties));

      dynamicType.Properties.Add(new PropertyType<string>("SomeProp"));
      var factory = dynamicType.CreateFactory();
      Assert.IsNotNull(factory);

      object instance = factory.CreateInstance();
      Assert.IsNotNull(instance);

      Assert.IsNotNull(instance as AbstractClass);
      Assert.IsNotNull(instance as IInterfaceWithProperties);

    }

    [Test]
    public void DynamicTypeBasedWithDifferentTargetAndBaseTypeUsage()
    {
      PropertyType<string> someProp = new PropertyType<string>("SomeProp");

      DynamicType<IInterfaceWithProperties> dynamicType = new DynamicType<IInterfaceWithProperties>("Test", typeof(AbstractClass), someProp);

      var factory = dynamicType.CreateFactory();

      IInterfaceWithProperties instance = factory.CreateInstance();
      Assert.IsNotNull(instance);

      instance.SetProperty(someProp, "Test1");
      Assert.AreEqual("Test1", instance.SomeProp);

    }

    [Test]
    [ExpectedException(typeof(InvalidCastException))]
    public void DynamicTypeBasedWithDifferentTargetAndBaseTypeUnsupported()
    {

      PropertyType<string> someProp = new PropertyType<string>("SomeProp");

      DynamicType<IInterfaceWithProperties> dynamicType = new DynamicType<IInterfaceWithProperties>("Test", typeof(AbstractClass), someProp);

      var factory = dynamicType.CreateFactory();

      AbstractClass instance = (AbstractClass)factory.CreateInstance();
      Assert.IsNotNull(instance);

      // this isn't supported, since the property accessor binder creates 
      //  delegates for accessing the property using IInterfaceWithProperties as the generic type parameter and NOT AbstractClass!
      instance.SetProperty(someProp, "Test1");
    
    }

    [Test]
    public void DynamicTypeOnAbstractTypeWithAlreadyImplementedProperty()
    {

      PropertyType<string> someProp = new PropertyType<string>("SomeProp");

      var dynamicType = new DynamicType<AbstractClassWithProperty>("Test", someProp);
      var factory = dynamicType.CreateFactory();

      var instance = factory.CreateInstance();
      Assert.IsNotNull(instance);

      instance.SetProperty(someProp, "Test1");
      Assert.AreEqual("Test1", instance.SomeProp);

    }

    [Test]
    public void DynamicTypeOnConcreteTypeWithAlreadyImplementedProperty()
    {

      PropertyType<string> someProp = new PropertyType<string>("SomeProp");

      var dynamicType = new DynamicType<ConcreteClassWithProperty>("Test", someProp);
        var factory = dynamicType.CreateFactory();

      var instance = factory.CreateInstance();
      Assert.IsNotNull(instance);

      instance.SetProperty(someProp, "Test1");
      Assert.AreEqual("Test1", instance.SomeProp);

    }

    [Test]
    public void SimpleDynamicType()
    {
     
      DynamicType<object> dynamicType = new DynamicType<object>();
      Assert.AreEqual(dynamicType.BaseType, typeof(object));

      dynamicType.Properties.Add(new PropertyType<string>("StringProp"));
      dynamicType.Properties.Add(new PropertyType<int>("Int32Prop"));
      dynamicType.Properties.Add(new PropertyType<DateTime>("DateTimeProp"));
      dynamicType.Properties.Add(new PropertyType<bool>("BoolProp"));
      
      IDynamicTypeFactory<object> factory = dynamicType.CreateFactory();
      Assert.IsNotNull(factory);

      IPropertyType stringProp = factory.Properties["StringProp"];
      Assert.IsNotNull(stringProp);

      IPropertyType int32Prop = factory.Properties["Int32Prop"];
      Assert.IsNotNull(int32Prop);

      IPropertyType dateTimeProp = factory.Properties["DateTimeProp"];
      Assert.IsNotNull(dateTimeProp);

      IPropertyType boolProp = factory.Properties["BoolProp"];
      Assert.IsNotNull(boolProp);

      object instance = factory.CreateInstance();
      Assert.IsNotNull(instance);

      instance.SetProperty(stringProp, "SomeStringValue");
      instance.SetProperty(int32Prop, 999);
      instance.SetProperty(dateTimeProp, new DateTime(2008, 11, 15));
      instance.SetProperty(boolProp, true);

      Assert.AreEqual("SomeStringValue", instance.GetProperty<object, string>(stringProp));
      Assert.AreEqual(999, instance.GetProperty<object, int>(int32Prop));
      Assert.AreEqual(new DateTime(2008, 11, 15), instance.GetProperty<object, DateTime>(dateTimeProp));
      Assert.AreEqual(true, instance.GetProperty<object, bool>(boolProp));
      
    }

    [Test]
    public void BetterSimpleDynamicType()
    {

      PropertyType<string> stringProp = new PropertyType<string>("StringProp");
      PropertyType<int> int32Prop = new PropertyType<int>("Int32Prop");
      PropertyType<DateTime> dateTimeProp = new PropertyType<DateTime>("DateTimeProp");
      PropertyType<bool> boolProp = new PropertyType<bool>("BoolProp");

      DynamicType<object> dynamicType = new DynamicType<object>
        (
          "Better",
          stringProp, int32Prop, dateTimeProp, boolProp
        );
      Assert.AreEqual(dynamicType.BaseType, typeof(object));

      IDynamicTypeFactory<object> factory = dynamicType.CreateFactory();
      Assert.IsNotNull(factory);

      object instance = factory.CreateInstance();
      Assert.IsNotNull(instance);

      instance.SetProperty(stringProp, "SomeStringValue");
      instance.SetProperty(int32Prop, 999);
      instance.SetProperty(dateTimeProp, new DateTime(2008, 11, 15));
      instance.SetProperty(boolProp, true);

      Assert.AreEqual("SomeStringValue", instance.GetProperty(stringProp));
      Assert.AreEqual(999, instance.GetProperty(int32Prop));
      Assert.AreEqual(new DateTime(2008, 11, 15), instance.GetProperty(dateTimeProp));
      Assert.AreEqual(true, instance.GetProperty(boolProp));

    }

    [Test]
    public void DynamicTypeWithMethods()
    {

      DynamicType<object> dynamicType = new DynamicType<object>();
      Assert.AreEqual(dynamicType.BaseType, typeof(object));

      // NOTE: the lambda passed in isn't actually called, it's used to get the method it calls.
      
      dynamicType.Methods.Add(new MethodType<Action>("Method1", () => SampleMethodLibrary.Method1()));
      dynamicType.Methods.Add(new MethodType<Action<TestGotHereHelper>>("Method2", (arg1) => SampleMethodLibrary.Method2(arg1)));
      dynamicType.Methods.Add(new MethodType<Func<TestGotHereHelper, int, bool>>("Method3", (arg1, arg2) => SampleMethodLibrary.Method3(arg1, arg2)));

      IDynamicTypeFactory<object> factory = dynamicType.CreateFactory();
      Assert.IsNotNull(factory);

      object instance = factory.CreateInstance();
      Assert.IsNotNull(instance);

      MethodType<Action> method1 = (MethodType<Action>) dynamicType.Methods["Method1"];
      MethodType<Action<TestGotHereHelper>> method2 = (MethodType<Action<TestGotHereHelper>>)dynamicType.Methods["Method2"];
      MethodType<Func<TestGotHereHelper, int, bool>> method3 = (MethodType<Func<TestGotHereHelper, int, bool>>)dynamicType.Methods["Method3"];

      // call methods
      Assert.DoesNotThrow(() => instance.InvokeMethod(method1));

      TestGotHereHelper helper2 = new TestGotHereHelper();
      instance.InvokeMethod(method2, helper2);
      Assert.AreEqual(true, helper2.WasHere);

      TestGotHereHelper helper3 = new TestGotHereHelper();
      bool success = (bool)instance.InvokeMethod(method3, helper3, 2);
      Assert.AreEqual(true, helper3.WasHere);
      Assert.AreEqual(true, success);
    }

    [Test]
    public void DynamicTypeWithMethodsWithIncorrectExpression()
    {

      // CAN'T DO THIS!!!

      DynamicType<object> dynamicType = new DynamicType<object>();
      Assert.AreEqual(dynamicType.BaseType, typeof(object));

      // Note: the lambda passed in isn't actually called, it's used to get the method it calls.
      dynamicType.Methods.Add(new MethodType<Action>("Method1", () => Console.WriteLine("Will never work!")));

      IDynamicTypeFactory<object> factory = dynamicType.CreateFactory();
      Assert.IsNotNull(factory);

      object instance = factory.CreateInstance();
      Assert.IsNotNull(instance);

      MethodType<Action> method1 = (MethodType<Action>)dynamicType.Methods["Method1"];
      Assert.Throws<TargetParameterCountException>(() => instance.InvokeMethod(method1));

    }

  }

  public static class SampleMethodLibrary
  {
    public static void Method1()
    {
      Console.WriteLine("Method1");
    }

    public static void Method2(TestGotHereHelper arg1)
    {
      Console.WriteLine("Method2");
      arg1.WasHere = true;
    }

    public static bool Method3(TestGotHereHelper arg1, int arg2)
    {
      Console.WriteLine("Method Imp3.");
      arg1.WasHere = true;
      return true;
    }
  }

  public class TestGotHereHelper
  {
    public bool WasHere { get; set; }
  }

}
