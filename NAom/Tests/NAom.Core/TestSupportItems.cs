﻿#region Copyright
// 
// Copyright (C) 2008 VirtualStaticVoid <virtualstaticvoid@gmail.com>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#endregion

namespace NAom.Core.Tests
{

  class Support
  {

    internal static DynamicType<TType> MakeDynamicType<TType>()
      where TType : class
    {
      return new DynamicType<TType>
        (
          typeof(TType).Name,
          InterfaceSupport.DerivePropertyTypes(typeof(TType))
        );
    }

  }

  class PrivateClass
  {
  }

  public interface IInterface
  {
  }

  public interface IInterfaceWithProperties
  {
    string SomeProp { get; set; }
  }

  public abstract class AbstractClass
  {
  }

  public abstract class AbstractClassWithAbstractMethod
  {
    public abstract void SomeMethod();
  }

  public abstract class AbstractClassWithAbstractProperty
  {
    public abstract string SomeProp { get; set; }
  }

  public abstract class AbstractClassWithProperty
  {
    public string SomeProp { get; set; }
  }

  public class ConcreteClassWithProperty
  {
    public string SomeProp { get; set; }
  }

  public class ConcreteClass
  {
  }

  public interface IGenericInterface<TType>
  {
    TType SomeProp { get; set; }
  }

  public interface IInterfaceBasedOnGenericInterface: IGenericInterface<string>
  {
  }

}
