﻿#region Copyright
// 
// Copyright (C) 2008 VirtualStaticVoid <virtualstaticvoid@gmail.com>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace NAom
{

  public class InterfaceCollection : Collection<Type>
  {

    protected override void InsertItem(int index, Type item)
    {
      if (item == null) throw new ArgumentNullException("item");
      if (!item.IsInterface)
        throw new NAomException(SR.ItemNotAnInterface(item));

      base.InsertItem(index, item);
    }

    protected override void SetItem(int index, Type item)
    {
      if (item == null) throw new ArgumentNullException("item");
      if (!item.IsInterface)
        throw new NAomException(SR.ItemNotAnInterface(item));

      base.SetItem(index, item);
    }

    public void AddRange(IEnumerable<Type> items)
    {
      if (items == null) throw new ArgumentNullException("items");
      foreach (Type item in items)
      {
        base.Add(item);
      }
    }

    public void AddRange(params Type[] items)
    {
      if (items == null) throw new ArgumentNullException("items");
      foreach (Type item in items)
      {
        base.Add(item);
      }
    }

    // fluent interface support
    new public InterfaceCollection Add(Type type)
    {
      base.Add(type);
      return this;
    }

    public ICollection<Type> AsReadOnly()
    {
      return new ReadOnlyCollection<Type>(this);
    }

  }

}
