﻿#region Copyright
// 
// Copyright (C) 2008 VirtualStaticVoid <virtualstaticvoid@gmail.com>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#endregion

using System;
using System.Collections.Generic;

namespace NAom
{
  [Serializable]
  public sealed class ReadOnlyNamedItemCollection<TItem> : IList<TItem>
    where TItem : class, INamedItem
  {

    private readonly NamedItemCollection<TItem> _items;

    public ReadOnlyNamedItemCollection(NamedItemCollection<TItem> items)
    {
      if (items == null) throw new ArgumentNullException("items");
      _items = items;
    }

    public TItem this[string key]
    {
      get
      {
        if (String.IsNullOrEmpty(key)) throw new ArgumentNullException("key");
        return _items[key];
      }
    }

    #region IList<TItem> Members

    public int IndexOf(TItem item)
    {
      if (item == null) throw new ArgumentNullException("item");
      return _items.IndexOf(item);
    }

    void IList<TItem>.Insert(int index, TItem item)
    {
      throw new NotSupportedException();
    }

    void IList<TItem>.RemoveAt(int index)
    {
      throw new NotSupportedException();
    }

    public TItem this[int index]
    {
      get
      {
        return _items[index];
      }
      set
      {
        throw new NotSupportedException();
      }
    }

    #endregion

    #region ICollection<TItem> Members

    void ICollection<TItem>.Add(TItem item)
    {
      throw new NotSupportedException();
    }

    void ICollection<TItem>.Clear()
    {
      throw new NotSupportedException();
    }

    public bool Contains(TItem item)
    {
      if (item == null) throw new ArgumentNullException("item");
      return _items.Contains(item);
    }

    public void CopyTo(TItem[] array, int arrayIndex)
    {
      if (array == null) throw new ArgumentNullException("array");
      _items.CopyTo(array, arrayIndex);
    }

    public int Count
    {
      get { return _items.Count; }
    }

    public bool IsReadOnly
    {
      get { return true; }
    }

    bool ICollection<TItem>.Remove(TItem item)
    {
      throw new NotSupportedException();
    }

    #endregion

    #region IEnumerable<TItem> Members

    public IEnumerator<TItem> GetEnumerator()
    {
      return _items.GetEnumerator();
    }

    #endregion

    #region IEnumerable Members

    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
      return _items.GetEnumerator();
    }

    #endregion
  }
}
