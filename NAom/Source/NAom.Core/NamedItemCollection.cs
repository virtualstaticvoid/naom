﻿#region Copyright
// 
// Copyright (C) 2008 VirtualStaticVoid <virtualstaticvoid@gmail.com>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace NAom
{
  [Serializable]
  public class NamedItemCollection<TItem> : KeyedCollection<string, TItem>
    where TItem : class, INamedItem
  {

    protected override string GetKeyForItem(TItem item)
    {
      if (item == null) throw new ArgumentNullException("item");
      return item.Name;
    }

    public void AddRange(IEnumerable<TItem> items)
    {
      if (items == null) throw new ArgumentNullException("items");
      foreach (TItem item in items)
      {
        base.Add(item);
      }
    }

    public void AddRange(params TItem[] items)
    {
      if (items == null) throw new ArgumentNullException("items");
      foreach (TItem item in items)
      {
        base.Add(item);
      }
    }

    // fluent interface support
    new public NamedItemCollection<TItem> Add(TItem item)
    {
      if (item == null) throw new ArgumentNullException("item");
      base.Add(item);
      return this;
    }

    public ReadOnlyNamedItemCollection<TItem> AsReadOnly()
    {
      return new ReadOnlyNamedItemCollection<TItem>(this);
    }

  }
}
