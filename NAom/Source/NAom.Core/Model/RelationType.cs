#region Copyright
// 
// Copyright (C) 2008 VirtualStaticVoid <virtualstaticvoid@gmail.com>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#endregion

using System;

namespace NAom.Model
{
  [Serializable]
  public class RelationType : ITypeDefinition
  {

    public RelationType(string name, EntityType leftEntityType, EntityType rightEntityType)
      : this(name, typeof(Relation), leftEntityType, rightEntityType)
    {
    }

    public RelationType(string name, Type baseType, EntityType leftEntityType, EntityType rightEntityType)
    {
      if (String.IsNullOrEmpty(name)) throw new ArgumentNullException("name");
      if (baseType == null) throw new ArgumentNullException("baseType");
      if (!typeof(Relation).IsAssignableFrom(baseType)) throw new ArgumentNullException("baseType");
      if (leftEntityType == null) throw new ArgumentNullException("leftEntityType");
      if (rightEntityType == null) throw new ArgumentNullException("rightEntityType");

      this.Name = name;
      this.BaseType = baseType;
      this.Interfaces = new InterfaceCollection();
      this.LeftEntityType = leftEntityType;
      this.RightEntityType = rightEntityType;
      this.AttributeTypes = new AttributeTypeCollection();
    }
    
    public string Name { get; private set; }
    public Type BaseType { get; private set; }
    public InterfaceCollection Interfaces { get; private set; }
    public EntityType LeftEntityType { get; private set; }
    public EntityType RightEntityType { get; private set; }
    public AttributeTypeCollection AttributeTypes { get; private set; }

    public override int GetHashCode()
    {
      return this.Name.GetHashCode();
    }

    public override bool Equals(object obj)
    {
      RelationType that = obj as RelationType;
      return (that != null && String.CompareOrdinal(this.Name, that.Name) == 0);
    }

  }
}