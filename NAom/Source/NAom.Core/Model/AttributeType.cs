#region Copyright
// 
// Copyright (C) 2008 VirtualStaticVoid <virtualstaticvoid@gmail.com>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#endregion

using System;
using NAom.Core;

namespace NAom.Model
{
  [Serializable]
  public class AttributeType : IAttributeType
  {

    public AttributeType(string name, Type dataType)
    {
      if (String.IsNullOrEmpty(name)) throw new ArgumentNullException("name");
      if (dataType == null) throw new ArgumentNullException("dataType");

      this.Name = name;
      this.DataType = dataType;
    }

    public string Name { get; private set; }
    public Type DataType { get; private set; }

    // "hide" by explictly implementing property
    IPropertyType IAttributeType.PropertyType { get; set; }

  }
}
