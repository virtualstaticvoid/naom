﻿#region Copyright
// 
// Copyright (C) 2008 VirtualStaticVoid <virtualstaticvoid@gmail.com>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#endregion

using System;
using System.Collections.Generic;

namespace NAom.Model
{
  public class AomModelBuilder
  {

    private readonly Dictionary<string, EntityType> _entityTypes;
    private readonly Dictionary<string, RelationType> _relationTypes;

    public AomModelBuilder()
    {
      _entityTypes = new Dictionary<string, EntityType>();
      _relationTypes = new Dictionary<string, RelationType>();
    }

    public void AddEntityType(EntityType entityType)
    {
      if (entityType == null) throw new ArgumentNullException("entityType");
      if (_entityTypes.ContainsKey(entityType.Name)) throw new ArgumentException(SR.DuplicateEntityType, "entityType");

      _entityTypes.Add(entityType.Name, entityType);
    }

    public void RemoveEntityType(EntityType entityType)
    {
      if (entityType == null) throw new ArgumentNullException("entityType");

      RemoveEntityType(entityType.Name);
    }

    public void RemoveEntityType(string entityTypeName)
    {
      if (String.IsNullOrEmpty(entityTypeName)) throw new ArgumentNullException("entityTypeName");

      _entityTypes.Remove(entityTypeName);
    }

    public void ClearEntityTypes()
    {
      _entityTypes.Clear();
    }

    internal IDictionary<string, EntityType> InternalEntityTypes
    {
      get { return _entityTypes; }
    }

    public IEnumerable<EntityType> EntityTypes
    {
      get { return _entityTypes.Values; }
    }

    public void AddRelationType(RelationType relationType)
    {
      if (relationType == null) throw new ArgumentNullException("relationType");
      if (_relationTypes.ContainsKey(relationType.Name)) throw new ArgumentException(SR.DuplicateRelationType, "relationType");

      _relationTypes.Add(relationType.Name, relationType);
    }

    public void RemoveRelationType(RelationType relationType)
    {
      if (relationType == null) throw new ArgumentNullException("relationType");

      RemoveRelationType(relationType.Name);
    }

    public void RemoveRelationType(string relationTypeName)
    {
      if (String.IsNullOrEmpty(relationTypeName)) throw new ArgumentNullException("relationTypeName");

      _relationTypes.Remove(relationTypeName);
    }

    public void ClearRelationTypes()
    {
      _relationTypes.Clear();
    }

    internal IDictionary<string, RelationType> InternalRelationTypes
    {
      get { return _relationTypes; }
    }

    public IEnumerable<RelationType> RelationTypes
    {
      get { return _relationTypes.Values; }
    }

    public AomModel Build()
    {
      return new AomModel(this);
    }

  }
}
