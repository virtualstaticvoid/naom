﻿#region Copyright
// 
// Copyright (C) 2008 VirtualStaticVoid <virtualstaticvoid@gmail.com>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#endregion

using System;

namespace NAom.Model
{
  public abstract class Entity : IAttributedMarker
  {

    protected Entity(EntityType entityType)
    {
      if (entityType == null) throw new ArgumentNullException("entityType");

      this.EntityType = entityType;
    }

    public EntityType EntityType { get; private set; }

    // properties, according to the attribute types of the entity type are added at runtime!

  }
}
