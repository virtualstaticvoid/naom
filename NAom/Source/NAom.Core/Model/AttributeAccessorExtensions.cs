﻿#region Copyright
// 
// Copyright (C) 2008 VirtualStaticVoid <virtualstaticvoid@gmail.com>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#endregion

using System;
using NAom.Core;

namespace NAom.Model
{
  public static class AttributeAccessorExtensions
  {

    public static TValue GetAttributeValue<TValue>(this IAttributedMarker instance, IAttributeType attributeType)
    {
      if (instance == null) throw new ArgumentNullException("instance");
      if (attributeType == null) throw new ArgumentNullException("attributeType");

      // ASSERT: attribute type belongs to instance
      return instance.GetProperty<IAttributedMarker, TValue>(attributeType.PropertyType);
    }

    public static void SetAttributeValue<TValue>(this IAttributedMarker instance, IAttributeType attributeType, TValue value)
    {
      if (instance == null) throw new ArgumentNullException("instance");
      if (attributeType == null) throw new ArgumentNullException("attributeType");

      // ASSERT: attribute type belongs to instance
      instance.SetProperty(attributeType.PropertyType, value);
    }

    public static object GetAttributeValue(this IAttributedMarker instance, IAttributeType attributeType)
    {
      if (instance == null) throw new ArgumentNullException("instance");
      if (attributeType == null) throw new ArgumentNullException("attributeType");

      // ASSERT: attribute type belongs to instance
      return instance.GetProperty(attributeType.PropertyType);
    }

    public static void SetAttributeValue(this IAttributedMarker instance, IAttributeType attributeType, object value)
    {
      if (instance == null) throw new ArgumentNullException("instance");
      if (attributeType == null) throw new ArgumentNullException("attributeType");

      // ASSERT: attribute type belongs to instance
      instance.SetProperty(attributeType.PropertyType, value);
    }

  }
}
