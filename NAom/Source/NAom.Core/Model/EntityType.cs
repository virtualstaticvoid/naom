#region Copyright
// 
// Copyright (C) 2008 VirtualStaticVoid <virtualstaticvoid@gmail.com>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#endregion

using System;

namespace NAom.Model
{
  [Serializable]
  public class EntityType : ITypeDefinition
  {

    public EntityType(string name)
      : this(name, typeof(Entity))
    {
    }
  
    public EntityType(string name, Type baseType)
    {
      if (String.IsNullOrEmpty(name)) throw new ArgumentNullException("name");
      if (baseType == null) throw new ArgumentNullException("baseType");
      if (!typeof(Entity).IsAssignableFrom(baseType)) throw new ArgumentNullException("baseType");

      this.Name = name;
      this.BaseType = baseType;
      this.Interfaces = new InterfaceCollection();
      this.AttributeTypes = new AttributeTypeCollection();
    }

    public string Name { get; private set; }
    public Type BaseType { get; private set; }
    public InterfaceCollection Interfaces { get; private set; }
    public AttributeTypeCollection AttributeTypes { get; private set; }

    public override int GetHashCode()
    {
      return this.Name.GetHashCode();
    }

    public override bool Equals(object obj)
    {
      EntityType that = obj as EntityType;
      return (that != null && String.CompareOrdinal(this.Name, that.Name) == 0);
    }
  
  }
}