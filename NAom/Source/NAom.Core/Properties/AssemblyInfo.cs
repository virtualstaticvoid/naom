#region Copyright
// 
// Copyright (C) 2008 VirtualStaticVoid <virtualstaticvoid@gmail.com>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#endregion

using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("NAom.Core")]
[assembly: AssemblyDescription("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// allow NAom.DAL access to the internals of NAom.Core
[assembly: InternalsVisibleTo("NAom.DAL, PublicKey=0024000004800000940000000602000000240000525341310004000001000100a9b1bd5f9f56b26e4e6f0374e005a0a7e05bcd1d698ff68b658cac7937bfc4cc1d08f18790a45d948b4b2693c6feb153001a8eabf51beb6b6ca1386e83db159d2f35cad090c0b18821f7a0da18ee476d76673aedc59d40043909f7162e09601b608961d4acad485583340b5f63d412afb6093d0b1f61f62774482b3a1c2c6094")]
