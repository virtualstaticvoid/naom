﻿#region Copyright
// 
// Copyright (C) 2008 VirtualStaticVoid <virtualstaticvoid@gmail.com>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#endregion

using System;

namespace NAom.Core
{
  public static class MethodTypeAccessorExtensions
  {
    public static object InvokeMethod<TTarget, TDelegate>(this TTarget target, MethodType<TDelegate> methodType)
      where TTarget : class
    {
      if (target == null) throw new ArgumentNullException("target");
      if (target as IGeneratedMarker == null) throw new NAomException(SR.InvalidTargetType(target.GetType()));
      if (methodType == null) throw new ArgumentNullException("methodType");

      return InternalInvokeMethod(target, methodType);
    }

    public static object InvokeMethod<TTarget, TDelegate, TArg1>(this TTarget target, MethodType<TDelegate> methodType, TArg1 arg1)
      where TTarget : class
    {
      if (target == null) throw new ArgumentNullException("target");
      if (target as IGeneratedMarker == null) throw new NAomException(SR.InvalidTargetType(target.GetType()));
      if (methodType == null) throw new ArgumentNullException("methodType");

      return InternalInvokeMethod(target, methodType, arg1);
    }

    public static object InvokeMethod<TTarget, TDelegate, TArg1, TArg2>(this TTarget target, MethodType<TDelegate> methodType, TArg1 arg1, TArg2 arg2)
      where TTarget : class
    {
      if (target == null) throw new ArgumentNullException("target");
      if (target as IGeneratedMarker == null) throw new NAomException(SR.InvalidTargetType(target.GetType()));
      if (methodType == null) throw new ArgumentNullException("methodType");

      return InternalInvokeMethod(target, methodType, arg1, arg2);
    }

    public static object InvokeMethod<TTarget, TDelegate, TArg1, TArg2, TArg3>(this TTarget target, MethodType<TDelegate> methodType, TArg1 arg1, TArg2 arg2, TArg3 arg3)
      where TTarget : class
    {
      if (target == null) throw new ArgumentNullException("target");
      if (target as IGeneratedMarker == null) throw new NAomException(SR.InvalidTargetType(target.GetType()));
      if (methodType == null) throw new ArgumentNullException("methodType");

      return InternalInvokeMethod(target, methodType, arg1, arg2, arg3);
    }

    public static object InvokeMethod<TTarget, TDelegate, TArg1, TArg2, TArg3, TArg4>(this TTarget target, MethodType<TDelegate> methodType, TArg1 arg1, TArg2 arg2, TArg3 arg3, TArg4 arg4)
      where TTarget : class
    {
      if (target == null) throw new ArgumentNullException("target");
      if (target as IGeneratedMarker == null) throw new NAomException(SR.InvalidTargetType(target.GetType()));
      if (methodType == null) throw new ArgumentNullException("methodType");

      return InternalInvokeMethod(target, methodType, arg1, arg2, arg3, arg4);
    }

    private static object InternalInvokeMethod<TTarget, TDelegate>(TTarget target, MethodType<TDelegate> methodType, params object[] args)
      where TTarget : class
    {
      if (target == null) throw new ArgumentNullException("target");
      if (target as IGeneratedMarker == null) throw new NAomException(SR.InvalidTargetType(target.GetType()));
      if (methodType == null) throw new ArgumentNullException("methodType");

      // TODO: argument type checking...

      return ((IInternalMethodType)methodType).MethodAccessor.Invoke(target, args);
    }

  }
}
