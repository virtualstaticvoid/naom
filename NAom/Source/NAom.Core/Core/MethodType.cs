#region Copyright
// 
// Copyright (C) 2008 VirtualStaticVoid <virtualstaticvoid@gmail.com>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#endregion

using System;
using System.Linq.Expressions;

namespace NAom.Core
{
  public class MethodType<TDelegate> : IInternalMethodType
    //where TDelegate: Delegate
  {

    // NOTE: the lambda expression passed in isn't actually called, it's used to get the method which it calls.

    public MethodType(string name, Expression<TDelegate> expression)
    {
      if (name == null) throw new ArgumentNullException("name");

      // validations as per MethodBuilder class
      if (name.Length == 0) throw new ArgumentException(SR.InvalidMethodName, "name");
      if (name[0] == '\0') throw new ArgumentException(SR.InvalidMethodName, "name");
      if (name.Length > 0x3ff) throw new ArgumentException(SR.InvalidMethodName, "name");

      Name = name;
      Expression = expression;
    }

    #region IMethod Members

    public string Name { get; private set; }

    public Expression Expression { get; private set; }

    #endregion

    #region IInternalMethodType

    private IMethodAccessor _methodAccessor;

    void IInternalMethodType.BindMethodAccessor(object methodAccessor)
    {
      _methodAccessor = (IMethodAccessor)methodAccessor;
    }

    bool IInternalMethodType.IsMethodAccessorBound
    {
      get { return _methodAccessor != null; }
    }

    IMethodAccessor IInternalMethodType.MethodAccessor
    {
      get { return _methodAccessor; }
    }

    #endregion

    // cast trick
    private IInternalMethodType InternalMethodType
    {
      get { return this; }
    } 

  }
}