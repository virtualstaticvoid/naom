#region Copyright
// 
// Copyright (C) 2008 VirtualStaticVoid <virtualstaticvoid@gmail.com>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#endregion

using System;
using System.Reflection.Emit;

namespace NAom.Core
{
  internal class FieldBuilderMapping
  {

    public FieldBuilderMapping(IInternalPropertyType propertyType, FieldBuilder fieldBuilder)
    {
      PropertyType = propertyType;
      FieldBuilder = fieldBuilder;
    }

    public IInternalPropertyType PropertyType { get; private set; }

    // NB: may be null, when base type already defines the property in question
    public FieldBuilder FieldBuilder { get; private set; }

    // shortcuts...

    public string PropertyName
    {
      get { return PropertyType.Name; }
    }

    public Type PropertyDataType
    {
      get { return PropertyType.DataType; }
    }

  }
}
