#region Copyright
// 
// Copyright (C) 2008 VirtualStaticVoid <virtualstaticvoid@gmail.com>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#endregion

using System;
using System.Reflection;
using System.Reflection.Emit;

namespace NAom.Core
{
  internal static class DynamicAssemblyHolder
  {

    public const string BASENAME = "NAom.DynamicAssembly";
    public const string TYPENAME_FORMAT = BASENAME + ".NAom_{0}_{1}_{2}";

    public static AssemblyBuilder AssemblyBuilder { get; private set; }

    public static ModuleBuilder ModuleBuilder { get; private set; }

    static DynamicAssemblyHolder()
    {

      //bool debugMode = false; // System.Diagnostics.Debugger.IsAttached;

      AssemblyName assemblyName = new AssemblyName(BASENAME);
      assemblyName.Version = new Version(1, 0);
      assemblyName.SetPublicKey(typeof(DynamicAssemblyHolder).Assembly.GetName().GetPublicKey());

      AssemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly
        (
          assemblyName,
          AssemblyBuilderAccess.Run /* debugMode ? AssemblyBuilderAccess.RunAndSave : AssemblyBuilderAccess.Run */
        );

      //if(debugMode)
      //  ModuleBuilder = AssemblyBuilder.DefineDynamicModule(BASENAME, BASENAME + ".dll", true);
      //else
        ModuleBuilder = AssemblyBuilder.DefineDynamicModule(BASENAME);

    }

  }
}
