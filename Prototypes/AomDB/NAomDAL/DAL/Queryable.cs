﻿#region Copyright
// 
// Copyright (C) 2008 VirtualStaticVoid <virtualstaticvoid@gmail.com>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#endregion

using System;
using System.Linq;
using System.Linq.Expressions;
using SysQueryable = System.Linq.Queryable;

namespace NAom.DAL
{
  public static class Queryable
  {

    // TODO: implement all extension methods of System.Linq.Queryable

    public static IQueryable<TSource> Where<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, int, bool>> predicate)
      where TSource: class, IEntity
    {
      if (source == null) throw new ArgumentNullException("source");
      if (predicate == null) throw new ArgumentNullException("predicate");
      IInternalEntityQueryable internalEntityQueryable = source as IInternalEntityQueryable;
      if (internalEntityQueryable == null) throw new InvalidCastException("Unexpected Type");

      return SysQueryable.Where(source, new AttributeTypeExpressionVisitor(internalEntityQueryable.GeneratedType).RewriteAttributeTypeExpression(predicate));
    }

    public static IQueryable<TSource> Where<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
      where TSource : class, IEntity
    {
      if (source == null) throw new ArgumentNullException("source");
      if (predicate == null) throw new ArgumentNullException("predicate");
      IInternalEntityQueryable internalEntityQueryable = source as IInternalEntityQueryable;
      if (internalEntityQueryable == null) throw new InvalidCastException("Unexpected Type");

      return SysQueryable.Where(source, new AttributeTypeExpressionVisitor(internalEntityQueryable.GeneratedType).RewriteAttributeTypeExpression(predicate));
    }

  }
}
