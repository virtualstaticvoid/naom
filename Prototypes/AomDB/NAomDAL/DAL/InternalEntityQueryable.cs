#region Copyright
// 
// Copyright (C) 2008 VirtualStaticVoid <virtualstaticvoid@gmail.com>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace NAom.DAL
{
  internal class InternalEntityQueryable<TEntity> : IInternalEntityQueryable, IQueryable<TEntity>
    where TEntity : class, IEntity 
  {

    private readonly Type _generatedType;
    private readonly IQueryable<TEntity> _innerQueryable;

    public InternalEntityQueryable(Type generatedType, IQueryable<TEntity> innerQueryable)
    {
      _generatedType = generatedType;
      _innerQueryable = innerQueryable;
    }

    #region IInternalEntityQueryable Members

    public Type GeneratedType
    {
      get { return _generatedType; }
    }

    #endregion

    #region IQueryable<TEntity> Members

    public IEnumerator<TEntity> GetEnumerator()
    {
      return _innerQueryable.GetEnumerator();
    }

    public Expression Expression
    {
      get { return _innerQueryable.Expression; }
    }

    public Type ElementType
    {
      get { return _generatedType; }
    }

    public IQueryProvider Provider
    {
      get { return _innerQueryable.Provider; }
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }

    #endregion

  }
}
