﻿#region Copyright
// 
// Copyright (C) 2008 VirtualStaticVoid <virtualstaticvoid@gmail.com>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#endregion

using NAom.Core;

namespace NAom.DAL
{
  public static class AttributeTypeExtensions
  {

    public static TValue GetProperty<TEntity, TValue>(this TEntity entity, IAttributeType attributeType)
      where TEntity : IEntity
    {
      IInternalAttributeType internalAttributeType = (IInternalAttributeType)attributeType;
      IInternalPropertyType internalPropertyType = (IInternalPropertyType)internalAttributeType.PropertyType;
      return (TValue)internalPropertyType.PropertyAccessor.GetPropertyValue(entity);
    }

    public static void SetProperty<TEntity, TValue>(this TEntity entity, IAttributeType attributeType, TValue value)
      where TEntity : IEntity
    {
      IInternalAttributeType internalAttributeType = (IInternalAttributeType)attributeType;
      IInternalPropertyType internalPropertyType = (IInternalPropertyType)internalAttributeType.PropertyType;
      internalPropertyType.PropertyAccessor.SetPropertyValue(entity, value);
    }

  }
}
