#region Copyright
// 
// Copyright (C) 2008 VirtualStaticVoid <virtualstaticvoid@gmail.com>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#endregion

using System;
using System.Xml.Linq;
using NAom.Core;

namespace NAom.DAL
{
  partial class EntityAttributeType : IAttributeType, IInternalAttributeType
  {

    public EntityAttributeType(string name, AttributeDataType dataType)
      : this()
    {
      Name = name;
      DataType = dataType;
    }

    public EntityAttributeType(string name, EntityType dataEntityType)
      : this()
    {
      Name = name;
      DataType = AttributeDataType.Entity;
      DataEntityType = dataEntityType;
    }

    #region IInternalAttributeType Members

    Type IInternalAttributeType.RealDataType
    {
      get
      {
        switch (DataType)
        {
          case AttributeDataType.Boolean:
            return typeof(bool?);
          case AttributeDataType.DateTime:
            return typeof(DateTime?);
          case AttributeDataType.Decimal:
            return typeof(decimal?);
          case AttributeDataType.Entity:
            return typeof(object);
          case AttributeDataType.Enum:
            return typeof(Enum);
          case AttributeDataType.Integer:
            return typeof(int?);
          case AttributeDataType.String:
            return typeof(string);
          case AttributeDataType.Xml:
            return typeof(XElement);
          default:
            throw new NotSupportedException();
        }
      }
    }

    private IPropertyType _propertyType;

    IPropertyType IInternalAttributeType.PropertyType
    {
      get { return _propertyType; }
      set { _propertyType = value; }
    }

    #endregion

  }
}