#region Copyright
// 
// Copyright (C) 2008 VirtualStaticVoid <virtualstaticvoid@gmail.com>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#endregion

namespace NAom.DAL
{
  partial class EntityType : IEntityType
  {

    public EntityType(string name)
      : this()
    {
      Name = name;
    }

    internal string GeneratedViewName
    {
      get { return string.Format("aomgen.Entity{0}", Name); }
    }

    public EntityRepository<IEntity> CreateEntityRepository()
    {
      return CreateEntityRepository<IEntity>();
    }

    public EntityRepository<TEntity> CreateEntityRepository<TEntity>()
      where TEntity : class, IEntity
    {
      return new EntityRepository<TEntity>(this);
    }

  }
}