/*
 * 
 * Copyright (C) 2008 VirtualStaticVoid <virtualstaticvoid@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http: *www.gnu.org/licenses/>.
 *
*/


use AomDB
go

if exists ( select * from sys.objects where object_id = object_id('aom.sp_generateRelationTypeView') and type = 'P' )
  drop procedure aom.sp_generateRelationTypeView
go

create procedure aom.sp_generateRelationTypeView
(
  @i_relationtypeid int
)
-- with execute as owner
as

  declare @sql nvarchar(MAX)

  declare @relationtypename nvarchar(50)
  declare @attributetypeid int
  declare @attributetypename nvarchar(50)
  declare @attributedatatype int
  declare @viewname nvarchar(60)

  select @relationtypename = Name
  from aom.RelationType
  where Id = @i_relationtypeid
  
  select @viewname = 'Relation' + @relationtypename
  
  select @sql = N'
if exists ( select * from sys.objects where object_id = object_id(''aomgen.[' + @viewname + ']'') and type = ''V'' )
      drop view aomgen.[' + @viewname + ']
'

  --print @sql
  exec sp_executesql @sql

  select @sql = N'
/*
 * GENERATED VIEW - DO NOT EDIT
 */
create view aomgen.[' + @viewname + ']
as

  select r.Id,
         r.Version,
         r.RelationTypeId,
         r.LeftEntityId,
         r.RightEntityId
'          

  declare cursorAttributeTypes cursor fast_forward
  for select Id, Name, DataType
      from aom.RelationAttributeType
      where RelationTypeId = @i_relationtypeid
      order by Name

  open cursorAttributeTypes

  fetch next from cursorAttributeTypes
  into @attributetypeid, @attributetypename, @attributedatatype

  while @@fetch_status = 0
  begin

    select @sql = @sql + N'           , aom.fn_getRelationAttribute' +  

    ( case @attributedatatype

        when 0 then 'Bool'
        when 1 then 'Int'
        when 2 then 'Decimal'
        when 3 then 'DateTime'
        when 4 then 'String'
        when 5 then 'Entity'
        when 6 then 'Xml'
        when 7 then 'Enum'
        else '!UNKNOWN!'

      end )

  + '( ' + convert(nvarchar, @attributetypeid) + ', r.Id ) as [' + @attributetypename + ']
'

    fetch next from cursorAttributeTypes
    into @attributetypeid, @attributetypename, @attributedatatype

  end

  close cursorAttributeTypes
  deallocate cursorAttributeTypes

  select @sql = @sql + N' 

  from aom.Relation r
  where r.RelationTypeId = ' + convert(nvarchar, @i_relationtypeid) + '  

'

  --print @sql
  exec sp_executesql @sql

  return @@error

go
