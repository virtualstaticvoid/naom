/*
 * 
 * Copyright (C) 2008 VirtualStaticVoid <virtualstaticvoid@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http: *www.gnu.org/licenses/>.
 *
*/


use AomDB
go

if exists ( select * from sys.objects where object_id = object_id('aom.trg_RelationAttributeType') and type = 'TR' )
  drop trigger aom.trg_RelationAttributeType
go

create trigger aom.trg_RelationAttributeType on aom.RelationAttributeType
after insert, update, delete
as
begin

  declare @relationtypeid int

  -- handle deletes
  
  declare cursorDeleted cursor fast_forward
  for select distinct RelationTypeId
      from deleted
      
  open cursorDeleted
  
  fetch next from cursorUpdated 
  into @relationtypeid
  
  while @@fetch_status = 0
  begin
  
    exec aom.sp_generateRelationTypeView @relationtypeid
    exec aom.sp_generateRelationTypeTrigger @relationtypeid
  
    fetch next from cursorDeleted 
    into @relationtypeid
    
  end
  
  close cursorDeleted
  deallocate cursorDeleted
  
  -- handle updates

  declare cursorUpdated cursor fast_forward
  for select distinct RelationTypeId
      from inserted 
      
  open cursorUpdated
  
  fetch next from cursorUpdated 
  into @relationtypeid
  
  while @@fetch_status = 0
  begin
  
    exec aom.sp_generateRelationTypeView @relationtypeid
    exec aom.sp_generateRelationTypeTrigger @relationtypeid
  
    fetch next from cursorUpdated 
    into @relationtypeid
    
  end
  
  close cursorUpdated
  deallocate cursorUpdated

end
go
