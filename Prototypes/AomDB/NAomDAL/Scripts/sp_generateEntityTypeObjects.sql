/*
 * 
 * Copyright (C) 2008 VirtualStaticVoid <virtualstaticvoid@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http: *www.gnu.org/licenses/>.
 *
*/


use AomDB
go

if exists ( select * from sys.objects where object_id = object_id('aom.sp_generateEntityTypeObjects') and type = 'P' )
  drop procedure aom.sp_generateEntityTypeObjects
go

create procedure aom.sp_generateEntityTypeObjects
-- with execute as owner
as

  declare @entitytypeid int

  declare cursorEntityTypes cursor fast_forward
  for select Id
      from aom.EntityType
      order by Name

  open cursorEntityTypes

  fetch next from cursorEntityTypes 
  into @entitytypeid

  while @@fetch_status = 0
  begin

    exec aom.sp_generateEntityTypeView @entitytypeid
    exec aom.sp_generateEntityTypeTrigger @entitytypeid
    
    fetch next from cursorEntityTypes 
    into @entitytypeid

  end

  close cursorEntityTypes
  deallocate cursorEntityTypes

  return @@error

go
